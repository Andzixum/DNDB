require 'discordrb'
require 'nationaldaylist'

n = NationalDayList.new(false)
bot = Discordrb::Bot.new token: '[TOKEN GOES HERE]'

bot.message(with_text: 'What day is it?') do |event|
	event.respond 'Doing some magic one sec.'
	s = ""
	t = Time.new
	d = n.get_month(t.month-1)[t.day-1].days 
	dc = 0
	while dc != d.length do
		s+="#{dc+1}) #{n.get_month(t.month-1)[t.day-1].days[dc].name}\n" 
		dc += 1
	end
	event.respond "Today there are #{d.length} national holidays:"
	event.respond s
	s = ""
end

bot.run
